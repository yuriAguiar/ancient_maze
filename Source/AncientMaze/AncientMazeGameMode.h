// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AncientMazeGameMode.generated.h"

UCLASS(minimalapi)
class AAncientMazeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAncientMazeGameMode();
};



