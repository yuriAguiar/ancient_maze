// Copyright Epic Games, Inc. All Rights Reserved.

#include "AncientMazeGameMode.h"
#include "AncientMazeBall.h"

AAncientMazeGameMode::AAncientMazeGameMode()
{
	// set default pawn class to our ball
	DefaultPawnClass = AAncientMazeBall::StaticClass();
}
